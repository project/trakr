<?php

namespace Drupal\trakr\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Exception\ClientException;

CONST TRAKR_BASE_URL = 'https://app.trakr.tech/';
CONST TRAKR_API_PROJECT = TRAKR_BASE_URL . 'api/v1/project/';
CONST TRAKR_API_DELETE_PROJECT = TRAKR_BASE_URL . 'api/v1/delete/project/';

/**
 * Defines a form that configures devel settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trakr_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'trakr.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {
    $trakr_config = $this->config('trakr.settings');

    $form['trakr_token'] = [
      '#type' => 'textfield',
      '#title' => t('API token'),
      '#default_value' => $trakr_config->get('trakr_token'),
      '#description' => $this->t('Your API token, you can find this information under your user profile in Trakr. If you do not have a Trakr user account, sign up at www.trakr.tech for free'),
      '#required' => TRUE,
    ];

    // Conventional field set.
    $form['trakr_project'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Trakr.tech'),
    ];

    if ($trakr_config->get('trakr_token') == '') {
      $form['trakr_project']['#description'] = $this->t('You have\'t configured an API token yet');
    } else {
      $trakr_project = $trakr_config->get('trakr_project');
      if (empty($trakr_project)) {
        // We have saved a token but have not sychronized with a project yet
        $form['trakr_project']['sync'] = [
          '#type' => 'submit',
          '#value' => $this->t('Link project on Trakr'),
          '#submit' => ['::syncProject'],
        ];
      } else {
        // We have an existing sync'ed project.
        // ping the project URL and make sure we have a 403, otherwise it looks like it's oprhaned
        $test = \Drupal::httpClient()->get($trakr_project['link'], ['http_errors' => False,]);
        if ($test->getStatusCode() != 403) {
          $trakr_config = \Drupal::service('config.factory')->getEditable('trakr.settings');
          $trakr_config->set('trakr_project', [])->save();
        }

        $form['trakr_token']['#disabled'] = True;
        $form['trakr_token']['#description'] = $this->t('You cannot change the API token until you have deleted the currently linked project below.');

        $content = [
          '#theme' => 'item_list',
          '#list_type' => 'ul',
          '#items' => [
            'Project name:' . $trakr_project['name'],
            'Notifications sent to: ' . $trakr_project['notification'],
            'Link:' . $trakr_project['link'],
          ],
          '#attributes' => ['class' => 'project_list'],
          '#wrapper_attributes' => ['class' => 'container'],
        ];
        $form['trakr_project']['details'] = [
          '#type' => 'details',
          '#title' => $this->t('Trakr project information'),
          '#description' => $content,
        ];

        $form['trakr_project']['delete'] = [
          '#type' => 'submit',
          '#value' => $this->t('Delete the project on Trakr'),
          '#submit' => ['::deleteProject'],
        ];

      }
    }

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configuration'),
      '#description' => $this->t('Submit, #type = submit'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Custom submission handler to delete a project
   */
  public function deleteProject(array &$form, FormStateInterface $form_state) {
    $project_info = $this->config('trakr.settings')->get('trakr_project');
    if (empty($project_info)) {
      $this->messenger()->addError($this->t('It appears there are is no Trakr project synchronized'));
    } else {
      // Extracting the project id from Trakr based on the project link
      $pieces = explode('/', $project_info['link']);
      $project_id = end($pieces);
      try {
          $response = \Drupal::httpClient()->post(
            TRAKR_API_DELETE_PROJECT . $project_id, [
            'headers' => [
              'x-api-key' => $form_state->getValue('trakr_token'),
              'Content-type' => 'text/plain',
            ]
          ]);
          $rsp_message = json_decode((string)$response->getBody(), True);
          $status = ($rsp_message['status'] == 'error') ? 'error' : 'status';
          $message = ($status == 'error') ? $rsp_message['message'] : $rsp_message['result'];

          // ksm($rsp_message);
          // Purge the project info
          if ($status != 'error') {
            $project_info = [];
            $trakr_config = \Drupal::service('config.factory')->getEditable('trakr.settings');
            $trakr_config->set('trakr_project', $project_info)->save();
          }
          $this->messenger()->addMessage($this->t('API response: @message', ['@message' => $message,]), $status);
      }
      catch(ClientException $e) {
        $form_state->setErrorByName('trakr_token', $e->getMessage());
      }
    }
  }

  /**
   * Our custom submission handler
   */
  public function syncProject(array &$form, FormStateInterface $form_state) {

    $site_config = $this->config('system.site');
    $project_api_params = [
      'production' => [
        'url' => $GLOBALS['base_url'],
      ],
      'uris' => [
        '/',
      ],
      'scan_url' => 1,
      'title' => $site_config->get('name'),
      'schedule' => [
        'interval' => 1,
        'comparison' => 1,
      ],
      'notification' => [
        'emails' => [
          $site_config->get('mail'),
        ],
      ],
    ];

    try {
        $response = \Drupal::httpClient()->post(
          TRAKR_API_PROJECT, [
          'body' => json_encode($project_api_params),
          'headers' => [
            'x-api-key' => $form_state->getValue('trakr_token'),
            'Content-type' => 'text/plain',
          ]
        ]);
        $rsp_message = json_decode((string)$response->getBody(), True);
        $status = ($rsp_message['status'] == 'error') ? 'error' : 'status';
        $message = ($status == 'error') ? $rsp_message['message'] : $rsp_message['result']['message'];

        // ksm($rsp_message);

        $project_info = [
          'notification' => $site_config->get('mail'),
          'name' => $site_config->get('name'),
          'link' => TRAKR_BASE_URL . 'node/' . $rsp_message['result']['id'],
        ];

        // Saving the synchronized project information
        $trakr_config = \Drupal::service('config.factory')->getEditable('trakr.settings');
        $trakr_config->set('trakr_project', $project_info)->save();

        $this->messenger()->addMessage($this->t('API response: @message', ['@message' => $message,]), $status);
    }
    catch(ClientException $e) {
      $form_state->setErrorByName('trakr_token', $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('trakr.settings')
      ->set('trakr_token', $values['trakr_token'])
      ->save();

    parent::submitForm($form, $form_state);
  }
}
