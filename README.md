I. About this module

This module allows you to create/link your Drupal website with a Trakr project and perform a visual comparison test on a daily basis.

II. Before you start

a. You will need a Trakr user account which you can obtain from http://www.trakr.tech
b. Your Drupal website should be publicly accessible via the internet (no intranet, proxy, etc)

III. How to use the module

1. Configuring the module
a. You can find the configuration page for Trakr under Administration > Configuration > Web Services
b. You can find the API token for Trakr under your user profile in Trakr.

For more information, contact support@trakr.tech
